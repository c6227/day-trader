"""
Request handlers related to logging
"""
from fastapi import APIRouter, Header, Response
from fastapi.responses import FileResponse
from ..util import DEFAULT_USER_ID
from ..log_queue import get_current_transaction_num, push_log_nodes, UserCommandTypeNode, log_queue

router = APIRouter()

# nathan: For dumping logs I propose we ignore "filename" because on a distributed system that's pretty dumb.
# The requests should respond with the logfile itself so the requestor can download it.


@router.get("/dumplog")
async def dump_log():
    """
    Dump the log of all transactions. No x-user-id header required.
    """
    user_command_type = UserCommandTypeNode(await get_current_transaction_num(), "DUMPLOG")
    await push_log_nodes([user_command_type])

    await log_queue.dump()

    return FileResponse("temp_log.xml")


@router.get("/dumplog/user")
async def dump_users_log(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Dump the log of user's transactions.
    """
    user_command_type = UserCommandTypeNode(await get_current_transaction_num(), "DUMPLOG")
    await push_log_nodes([user_command_type])

    await log_queue.dump(x_user_id)

    return FileResponse("temp_log.xml")
