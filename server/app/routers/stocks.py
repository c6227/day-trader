"""
Quote server API calls
"""
from time import time
from fastapi import APIRouter, Header, HTTPException
from ..quote_server import get_quote
from ..util import DEFAULT_USER_ID
from ..databases import db_clients
from ..log_queue import (
    AccountTransactionTypeNode,
    UserCommandTypeNode,
    QuoteServerTypeNode,
    push_log_nodes,
    get_current_transaction_num,
    increment_transaction_num
)

router = APIRouter()


@router.get("/quote/{symbol}")
async def quote(symbol: str, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Get a quote from the quote server.
    """
    db = db_clients.mongodb.daytrader

    quoted = await get_quote(symbol, x_user_id)

    if quoted.used_quote_server:
        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "QUOTE", x_user_id)
        quote_server_access = QuoteServerTypeNode(
            await get_current_transaction_num(), quoted.price, symbol, x_user_id, quoted.timestamp, quoted.crypto_key
        )
        await push_log_nodes([user_cmd, quote_server_access])
    else:
        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "QUOTE", x_user_id)
        await push_log_nodes([user_cmd])

    return {
        "price": quoted.price,
        "timestamp": quoted.timestamp,
        "crypto_key": quoted.crypto_key,
    }


@router.post("/buy/{symbol}/{amount}")
async def buy(symbol: str, amount: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Stage a buy for a DOLLAR AMOUNT of a stock of the given symbol.
    """
    db = db_clients.mongodb.daytrader

    # get funds value
    funds = await db.users.find_one({"userID": x_user_id})
    funds = funds["funds"]

    # log the command (it must be logged whether it failed or not)
    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "BUY", x_user_id, symbol)
    await push_log_nodes([user_cmd])

    # check to see if user has enough money to buy
    if amount > funds:
        # error
        raise HTTPException(status_code=400, detail="Not enough funds")

    # clear any buys that weren't acted on
    await db.buy.delete_one({"userID": x_user_id})
    # add buy to the buy collection
    await db.buy.insert_one(
        {"userID": x_user_id, "symbol": symbol, "amount": amount, "time": time()}
    )

    return {"symbol": symbol, "amount": amount}


@router.post("/buy/commit")
async def commit_buy(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Commit the most recent buy request.
    """
    db = db_clients.mongodb.daytrader

    check = await db.users.find_one({"userID": x_user_id})
    funds = check["funds"]

    # log the command (it must be logged whether it failed or not)
    transaction_num = await get_current_transaction_num()

    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "COMMIT_BUY", x_user_id)

    try:
        buying = await db.buy.find_one({"userID": x_user_id})
        symbol = buying["symbol"]
        amount = buying["amount"]
        timed = buying["time"]
    except TypeError:
        # there is no pending buy
        await push_log_nodes([user_cmd])
        return {
            "symbol": "",  # can be None if there was no buy in the last 60 seconds
            "amount_purchased": 0,
            "new_account_balance": funds,
        }

    await db.buy.delete_one({"userID": x_user_id})

    try:
        check = await db.stocksOwned.find_one({"userID": x_user_id, "symbol": symbol})
        # if they own that stock already
        # check if symbol is in the stock collection
        if check["symbol"] == symbol:
            if time() - timed <= 60:
                temp = check["amount"] + amount
                await db.stocksOwned.update_one(
                    {"_id": check["_id"]}, {"$set": {"amount": temp}}
                )
                # deduct price of purchased amount
                await db.users.update_one(
                    {"userID": x_user_id}, {"$set": {"funds": funds - amount}}
                )

                # log the transaction
                await increment_transaction_num()
                transaction_num = await get_current_transaction_num()
                user_cmd = UserCommandTypeNode(transaction_num, "COMMIT_BUY", x_user_id)
                log_transaction = AccountTransactionTypeNode(transaction_num, "BUY", x_user_id, amount)
                await push_log_nodes([user_cmd, log_transaction])

                return {
                    "symbol": symbol,
                    "amount_purchased": amount,
                    "new_account_balance": funds - amount,
                }
            else:
                await push_log_nodes([user_cmd])
                return {
                    "symbol": "",  # can be None if there was no buy in the last 60 seconds
                    "amount_purchased": 0,
                    "new_account_balance": funds,
                }
    except TypeError:
        if time() - timed <= 60:
            # if no, add it to collection, return
            # add stock symbol and amount to the users account
            await db.stocksOwned.insert_one(
                {"userID": x_user_id, "symbol": symbol, "amount": amount}
            )
            # deduct price of purchased amount
            await db.users.update_one(
                {"userID": x_user_id}, {"$set": {"funds": funds - amount}}
            )

            # log the transaction
            await increment_transaction_num()
            transaction_num = await get_current_transaction_num()
            user_cmd = UserCommandTypeNode(transaction_num, "COMMIT_BUY", x_user_id)
            log_transaction = AccountTransactionTypeNode(transaction_num, "BUY", x_user_id, amount)
            await push_log_nodes([user_cmd, log_transaction])

            return {
                "symbol": symbol,
                "amount_purchased": amount,
                "new_account_balance": funds - amount,
            }
        else:
            await push_log_nodes([user_cmd])
            return {
                "symbol": "",  # can be None if there was no buy in the last 60 seconds
                "amount_purchased": 0,
                "new_account_balance": funds,
            }


@router.post("/buy/cancel")
async def cancel_buy(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Cancel the most recent buy request.
    """
    db = db_clients.mongodb.daytrader

    buying = await db.buy.find_one({"userID": x_user_id})

    try:
        symbol = buying["symbol"]
        amount = buying["amount"]
        timed = buying["time"]
        if time() - timed <= 60:
            await db.buy.delete_one({"userID": x_user_id})

            # log of a successful buy cancel
            user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_BUY", x_user_id, funds=amount)
            await push_log_nodes([user_cmd])

            return {"cancelled_symbol": symbol, "cancelled_amount": amount}
        else:
            # log of an unsuccessful buy cancel
            user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_BUY", x_user_id)
            await push_log_nodes([user_cmd])

            return {
                "cancelled_symbol": "",  # can be None if there was no buy in the last 60 seconds
                "cancelled_amount": 0,
            }
    except TypeError:

        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_BUY", x_user_id)
        await push_log_nodes([user_cmd])

        return {
            "cancelled_symbol": "",  # can be None if there was no buy in the last 60 seconds
            "cancelled_amount": 0,
        }


@router.post("/sell/{symbol}/{amount}")
async def sell(symbol: str, amount: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Stage a sell for a DOLLAR AMOUNT of a stock of the given symbol.
    """
    db = db_clients.mongodb.daytrader

    # As in BUY, the command is logged the same way regardless of the outcome
    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "SELL", x_user_id, symbol, amount)
    await push_log_nodes([user_cmd])

    try:
        check = await db.stocksOwned.find_one({"userID": x_user_id, "symbol": symbol})
        if check["symbol"] == symbol:
            if check["amount"] > amount:
                # clear any buys that weren't acted on
                await db.sell.delete_one({"userID": x_user_id})
                # add buy to the buy collection
                await db.sell.insert_one(
                    {
                        "userID": x_user_id,
                        "symbol": symbol,
                        "amount": amount,
                        "time": time(),
                    }
                )

                return {"symbol": symbol, "amount": amount}
            else:
                # error
                raise HTTPException(status_code=400, detail="Not enough stock owned")
    except TypeError:
        # error
        raise HTTPException(status_code=400, detail="Stock not owned")


@router.post("/sell/commit")
async def commit_sell(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Commit the most recent sell request.
    """
    db = db_clients.mongodb.daytrader

    # potential transaction number
    transaction_num = await get_current_transaction_num()

    funds = await db.users.find_one({"userID": x_user_id})
    funds = funds["funds"]
    try:
        selling = await db.sell.find_one({"userID": x_user_id})
        symbol = selling["symbol"]
        amount = selling["amount"]
        timed = selling["time"]
    except TypeError:
        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "COMMIT_SELL", x_user_id)
        await push_log_nodes([user_cmd])

        return {
            "symbol": "",  # can be None if there was no buy in the last 60 seconds
            "amount_sold": 0,
            "new_account_balance": funds,
        }

    await db.sell.delete_one({"userID": x_user_id})

    if time() - timed <= 60:
        check = await db.stocksOwned.find_one({"userID": x_user_id, "symbol": symbol})
        await db.stocksOwned.update_one(
            {"_id": check["_id"]}, {"$set": {"amount": funds + amount}}
        )
        # Add price of stocks sold
        db.users.update_one({"userID": x_user_id}, {"$set": {"funds": funds + amount}})

        # log the transaction
        await increment_transaction_num()
        transaction_num = await get_current_transaction_num()
        user_cmd = UserCommandTypeNode(transaction_num, "COMMIT_SELL", x_user_id)
        transaction = AccountTransactionTypeNode(transaction_num, "SELL", x_user_id, amount)
        await push_log_nodes([user_cmd, transaction])

        return {
            "symbol": symbol,  # should be None if there was no sell in the last 60 seconds
            "amount_sold": amount,
            "new_account_balance": funds + amount,
        }
    else:
        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "COMMIT_SELL", x_user_id)
        await push_log_nodes([user_cmd])
        return {
            "symbol": "",  # should be None if there was no sell in the last 60 seconds
            "amount_sold": 0,
            "new_account_balance": funds,
        }


@router.post("/sell/cancel")
async def cancel_sell(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Cancel the most recent sell request.
    """
    db = db_clients.mongodb.daytrader

    selling = await db.sell.find_one({"userID": x_user_id})
    try:
        symbol = selling["symbol"]
        amount = selling["amount"]
        timed = selling["time"]
        if time() - timed <= 60:
            await db.sell.delete_one({"userID": x_user_id})

            # log of a successful sell cancel
            user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_SELL", x_user_id, funds=amount)
            await push_log_nodes([user_cmd])

            return {"cancelled_symbol": symbol, "cancelled_amount": amount}
        else:

            # log of an unsuccessful sell cancel
            user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_SELL", x_user_id)
            await push_log_nodes([user_cmd])

            return {
                "cancelled_symbol": "",  # can be None if there was no buy in the last 60 seconds
                "cancelled_amount": 0,
            }
    except TypeError:

        # log of an unsuccessful sell cancel
        user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "CANCEL_SELL", x_user_id)
        await push_log_nodes([user_cmd])

        return {
            "cancelled_symbol": "",  # can be None if there was no buy in the last 60 seconds
            "cancelled_amount": 0,
        }
