"""
Router for auto buy and sell requests
"""
from fastapi import APIRouter, Header
from fastapi.exceptions import HTTPException
from ..util import DEFAULT_USER_ID
from ..databases import db_clients
from ..log_queue import (
    AccountTransactionTypeNode,
    UserCommandTypeNode,
    push_log_nodes,
    get_current_transaction_num,
    increment_transaction_num
)

router = APIRouter()


@router.post("/buy/quantity/{symbol}/{amount}")
async def buy_quantity(symbol: str, amount: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Set the DOLLAR AMOUNT of 'symbol' to buy when the stock is <= the trigger price set by
    SET_BUY_TRIGGER.

    Command: SET_BUY_AMOUNT
    """
    db = db_clients.mongodb.daytrader

    # an example of how to "fail" with FastAPI
    # raise HTTPException(status_code=400, detail="There was no buy target price set.")

    # checks that the user has sufficient funds to stage this purchase
    user = await db.users.find_one({"userID": x_user_id})
    #logging
    transaction_num = await get_current_transaction_num()

    if user is None:
        raise HTTPException(status_code=400, detail="This user does not exist within the database.")
    if user["funds"] < amount:
        raise HTTPException(status_code=400, detail="Not enough funds to stage this purchase.")
    # check that there isn't an amount staged for this stock already
    existing_amount = await db.buyTriggers.find_one({"userID": x_user_id, "symbol": symbol})
    if existing_amount:
        raise HTTPException(status_code=400, detail="Must cancel the previous stage before making a new one.")
    # NOTE: active field serves as a way to only query active triggers in another function, instead of staged ones
    await db.buyTriggers.insert_one({"userID": x_user_id, "symbol": symbol, "amount": amount, "active": 0})
    # stages the amount of money to be spent from the user's funds
    await db.users.update_one({"userID":x_user_id}, {"$inc": {"funds": (-1*amount)}})

    await increment_transaction_num()
    transaction_num = await get_current_transaction_num()
    user_cmd = UserCommandTypeNode(transaction_num, "SET_BUY_AMOUNT", x_user_id, symbol)
    transaction = AccountTransactionTypeNode(transaction_num, "remove", x_user_id, amount)
    await push_log_nodes([user_cmd, transaction])

    return {
        "symbol": symbol,
        "amount": amount,
        "new_account_balance": (user["funds"] - amount)
    }


@router.post("/buy/target/{symbol}/{price}")
async def buy_target_price(symbol: str, price: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Set to buy "symbol" when the stock price is <= 'price'. Command: SET_BUY_TRIGGER
    Responds with the symbol and the price as they were given.

    This should fail if the user has not set a target amount with SET_BUY_AMOUNT.
    """
    db = db_clients.mongodb.daytrader

    # checks that the user has set a target amount with SET_BUY_AMOUNT
    trigger = await db.buyTriggers.find_one({"userID": x_user_id, "symbol": symbol})
    if trigger is None:
        raise HTTPException(status_code=400, detail="There was no buy target amount set")
    elif trigger["amount"] < price:
        raise HTTPException(status_code=400, detail="target price cannot be higher than the amount staged for this stock")
    
    await db.buyTriggers.update_one({"_id": trigger["_id"]}, {"$set": {"trigger": price, "active": 1}})
    
    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "SET_BUY_TRIGGER", x_user_id, symbol)
    await push_log_nodes([user_cmd])

    return {
        "symbol": symbol,
        "price": price
    }


@router.post("/buy/cancel/{symbol}")
async def buy_cancel(symbol: str, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Cancel a SET_BUY command. Command: CANCEL_SET_BUY
    Responds with the symbol and amount of the cancelled auto-buy, as well as the updated account balance.
    """
    db = db_clients.mongodb.daytrader

    # checks that the specified user has a staged auto-buy
    buy_trigger = await db.buyTriggers.find_one({"userID": DEFAULT_USER_ID, "symbol": symbol})
    if buy_trigger is None:
        raise HTTPException(status_code=400, detail="No staged purchase to cancel")

    transaction_num = await get_current_transaction_num()

    # returns the staged money back to the user's main funds
    fundsReturned = buy_trigger["amount"]
    await db.users.update_one({"userID": DEFAULT_USER_ID}, {"$inc": {"funds": fundsReturned}})

    # returns new account balance
    user = await db.users.find_one({"userID": DEFAULT_USER_ID})
    new_balance = user["funds"]

    # deletes old buy_trigger
    await db.buyTriggers.delete_one({"userID": DEFAULT_USER_ID, "symbol": symbol})

    await increment_transaction_num()
    transaction_num = await get_current_transaction_num()
    user_cmd = UserCommandTypeNode(transaction_num, "CANCEL_SET_BUY", x_user_id, symbol)
    transaction = AccountTransactionTypeNode(transaction_num, "add", x_user_id, fundsReturned)
    await push_log_nodes([user_cmd, transaction])

    return {
        "symbol": symbol,
        "amount_returned": fundsReturned,
        "new_account_balance": new_balance
    }


@router.post("/sell/quantity/{symbol}/{amount}")
async def sell_quantity(symbol: str, amount: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Set the STOCK amount of 'symbol' to sell when the stock price is >= the trigger price set by
    SET_SELL_TRIGGER.

    Command: SET_SELL_AMOUNT
    """
    db = db_clients.mongodb.daytrader

    # checks that the user has sufficient stock in this symbol
    stock_owned = await db.stocksOwned.find_one({"userID": x_user_id, "symbol": symbol})
    #logging
    transaction_num = await get_current_transaction_num()

    if (stock_owned is None or stock_owned["amount"] < amount):
        raise HTTPException(status_code=400, detail="Selling more stock than owned.")
    # check that there isn't an amount staged for this stock already
    existing_amount = await db.sellTriggers.find_one({"userID": x_user_id, "symbol": symbol})
    if existing_amount:
        raise HTTPException(status_code=400, detail="Must cancel the previous stage before making a new one.")
    # NOTE: active field serves as a way to only query active triggers in another function, instead of staged ones
    await db.sellTriggers.insert_one({"userID": x_user_id, "symbol": symbol, "amount": amount, "active": 0})
    # stages the amount of stock to be removed from the user's stock count
    await db.stocksOwned.update_one({"userID": x_user_id, "symbol": symbol}, {"$inc": {"amount": (-1*amount)}})

    user_cmd = UserCommandTypeNode(transaction_num, "SET_SELL_AMOUNT", x_user_id, symbol)
    await push_log_nodes([user_cmd])

    return {
        "symbol": symbol,
        "stocks_to_sell": amount,
        "total_stocks": stock_owned["amount"]
    }


@router.post("/sell/target/{symbol}/{price}")
async def sell_target_price(symbol: str, price: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Set to sell 'symbol' when the stock price is >= 'price'. Command: SET_SELL_TRIGGER

    This should fail if the user has not set a target price with SET_SELL_AMOUNT.
    """
    db = db_clients.mongodb.daytrader

    # checks that the user has set a target amount with SET_BUY_AMOUNT
    trigger = await db.sellTriggers.find_one({"userID": x_user_id, "symbol": symbol})
    if trigger is None:
        raise HTTPException(status_code=400, detail="There was no sell target amount set")
    
    await db.sellTriggers.update_one({"_id": trigger["_id"]}, {"$set": {"trigger": price, "active": 1}})
    amount = trigger["amount"]

    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "SET_SELL_TRIGGER", x_user_id, symbol)
    await push_log_nodes([user_cmd])

    return {
        "symbol": symbol,
        "amount_of_stocks_to_sell": amount,
        "minimum_stock_sell_price": price,
        "minimum_sell_order_total": amount * price
    }


@router.post("/sell/cancel/{symbol}")
async def sell_cancel(symbol: str, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Cancel a SET_SELL command. Command: CANCEL_SET_SELL
    """
    db = db_clients.mongodb.daytrader

    # checks that the specified user has a staged auto-sell
    sell_trigger = await db.sellTriggers.find_one({"userID": DEFAULT_USER_ID, "symbol": symbol})
    if sell_trigger is None:
        raise HTTPException(status_code=400, detail="No staged sell to cancel")

    transaction_num = await get_current_transaction_num()

    # returns the staged stocks back to the user's owned stocks
    stock_returned = sell_trigger["amount"]
    await db.stocksOwned.update_one({"userID": DEFAULT_USER_ID, "symbol": symbol}, {"$inc": {"amount": stock_returned}})

    # returns new stock_owned balance
    stock_owned = await db.stocksOwned.find_one({"userID": DEFAULT_USER_ID, "symbol": symbol})
    newStockBalance = stock_owned["amount"]

    # deletes old sell_trigger
    await db.sellTriggers.delete_one({"userID": DEFAULT_USER_ID, "symbol": symbol})

    user_cmd = UserCommandTypeNode(transaction_num, "CANCEL_SET_SELL", x_user_id, symbol)
    await push_log_nodes([user_cmd])

    return {
        "symbol": symbol,
        "stocks_returned": stock_returned,
        "total_stocks_left": newStockBalance  # updated amount of stocks in the account now that the sell order was cancelled
    }