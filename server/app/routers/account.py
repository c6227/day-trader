"""
Routes related to managing funds
"""

from fastapi import APIRouter, Header
from ..util import DEFAULT_USER_ID

from ..databases import db_clients
from ..log_queue import (
    get_current_transaction_num,
    increment_transaction_num,
    UserCommandTypeNode,
    AccountTransactionTypeNode,
    push_log_nodes,
)

router = APIRouter()


@router.post("/add/{amount}")
async def add_funds(amount: float, x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Add funds to the user's account.
    CLI Command: ADD
    Responds with funds_added and a float confirming the amount added.
    """
    db = db_clients.mongodb.daytrader
    await db.users.update_one({"userID": x_user_id}, {"$inc": {"funds": amount}})

    # logging
    await increment_transaction_num()
    transaction_num = await get_current_transaction_num()
    user_cmd = UserCommandTypeNode(transaction_num, "ADD", x_user_id, funds=amount)
    account_trans = AccountTransactionTypeNode(
        transaction_num, "ADD", x_user_id, amount
    )
    await push_log_nodes([user_cmd, account_trans])

    return {"funds_added": amount}


@router.get("/summary")
async def account_summary(x_user_id: str = Header(DEFAULT_USER_ID)):
    """
    Provides a summary to the client of the given user's transaction history and
    the current status of their accounts as well as any set buy or sell triggers and their parameters.
    Responds with (see below)
    """
    db = db_clients.mongodb.daytrader
    funds = await db.users.find_one({"userID": x_user_id})

    buy_cursor = db.buyTriggers.find({"userID": x_user_id}, {"_id": 0})
    buy_triggers = []
    for document in await buy_cursor.to_list(length=None):
        buy_triggers.append(document)

    sell_cursor = db.sellTriggers.find({"userID": x_user_id}, {"_id": 0})
    sell_triggers = []
    for document in await sell_cursor.to_list(length=None):
        sell_triggers.append(document)

    stocks_owned = db.stocksOwned.find({"userID": x_user_id}, {"_id": 0})
    stocks = []
    for document in await stocks_owned.to_list(length=None):
        stocks.append(document)

    # logging
    user_cmd = UserCommandTypeNode(await get_current_transaction_num(), "DISPLAY_SUMMARY", x_user_id)
    await push_log_nodes([user_cmd])

    return {
        "current_balance": funds["funds"],
        "transaction_history": [],
        "buy_triggers": buy_triggers,
        "sell_triggers": sell_triggers,
        "stocks_owned": stocks,
    }
