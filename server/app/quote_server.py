"""
Provides async functions to obtain a quote from the quote server.

The IP and port must be specified in environment variables:

DT_QUOTESERV_IP
DT_QUOTESERV_PORT
"""

import asyncio
from time import time
from os import environ
from .databases import db_clients

_QUOTESERVER_RESPONSE_TIMEOUT = 30
_MAXIMUM_PRICE_CACHE_TIME = 30


class QuoteServerResponse:
    """
    A structured response from get_quote
    """

    def __init__(self, price: float, timestamp: str, crypto_key: str, used_quote_server: bool):
        self.price = price
        self.timestamp = timestamp
        self.crypto_key = crypto_key
        self.used_quote_server = used_quote_server


async def get_quote(symbol: str, user_id: str):
    """
    Query the quote server for a stock price.

    If the quote server does not respond in time, asyncio.TimeoutError is raised.

    :param symbol: the stock symbol to query the quoteserver for
    :param user_id:
    :return: a QuoteServerResponse
    :raises asyncio.TimeoutError if the quote server does not respond in time
    :raises IOError if there is a problem with the TCP connection
    """
    redis = db_clients.redis

    # check if there is a cached stock price
    cached_price = await redis.get(f"{symbol}_PRICE")

    if cached_price:
        cached_time = await redis.get(f"{symbol}_TIME")
        if cached_time and time() - int(cached_time) <= _MAXIMUM_PRICE_CACHE_TIME:
            # valid cached stock price (less than 30 seconds)
            crypto_key = await redis.get(f"{symbol}_CRYPTOKEY")
            return QuoteServerResponse(
                float(cached_price), str(int(cached_time) * 1000), crypto_key, False
            )

    # no cached price, query the legacy server
    reader, writer = await asyncio.open_connection(
        environ["DT_QUOTESERV_IP"], int(environ["DT_QUOTESERV_PORT"])
    )
    writer.write(f"{symbol} {user_id}\n".encode())

    response = (
        (await asyncio.wait_for(reader.readuntil(b"\n"), _QUOTESERVER_RESPONSE_TIMEOUT))
        .decode()
        .strip()
        .split(",")
    )

    writer.close()

    quote_response = QuoteServerResponse(
        float(response[0]), response[3], response[4], True
    )

    # cache the new price
    await redis.set(f"{symbol}_PRICE", quote_response.price)
    await redis.set(f"{symbol}_TIME", int(int(quote_response.timestamp) / 1000))
    await redis.set(f"{symbol}_CRYPTOKEY", quote_response.crypto_key)

    return quote_response
