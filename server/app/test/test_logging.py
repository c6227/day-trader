from fastapi.testclient import TestClient

from ..main import app

TEST_USER_HEADER1 = {"x-user-id": "logging_tests_user1"}
TEST_USER_HEADER2 = {"x-user-id": "logging_tests_user2"}


def test_logging():
    """
    Very simple test to verify a log is dumped for each user and globally.
    """
    with TestClient(app) as client:
        # do some quotes
        client.get("/stocks/quote/ACA", headers=TEST_USER_HEADER1)
        client.get("/stocks/quote/ACA", headers=TEST_USER_HEADER2)
        client.get("/stocks/quote/PPU", headers=TEST_USER_HEADER1)
        client.get("/stocks/quote/PPU", headers=TEST_USER_HEADER2)

        # dump the logs (these should be xml strings)
        response1 = client.get("/logging/dumplog/user", headers=TEST_USER_HEADER1).content.decode()
        response2 = client.get("/logging/dumplog/user", headers=TEST_USER_HEADER2).content.decode()
        response_global = client.get("/logging/dumplog").content.decode()

        # check the logs
        assert "<username>logging_tests_user1</username>" in response1
        assert "<username>logging_tests_user2</username>" in response2
        assert (
            "<username>logging_tests_user1</username>" in response_global
            and "<username>logging_tests_user2</username>" in response_global
        )

        # check there is no error parsing the xml
        import xml.etree.ElementTree as ElementTree
        ElementTree.fromstring(response1)
        ElementTree.fromstring(response2)
        ElementTree.fromstring(response_global)
