import pytest
from fastapi.testclient import TestClient

from ..main import app

TEST_USER_HEADER = {"x-user-id": "account_tests_user"}


@pytest.mark.order(1)
def test_add_funds():
    """
    Tests adding funds to an account
    """
    with TestClient(app) as client:
        response = client.post("/account/add/123.4", headers=TEST_USER_HEADER)
        assert response.json() == {"funds_added": 123.4}


@pytest.mark.order(2)
def test_account_summary():
    """
    Empty account summary with balance
    """
    with TestClient(app) as client:
        response = client.get("/account/summary", headers=TEST_USER_HEADER)
        assert response.json() == {
            "current_balance": 123.4,
            "transaction_history": [],
            "buy_triggers": [],
            "sell_triggers": [],
            "stocks_owned": [],
        }
