from fastapi.testclient import TestClient

from ..main import app

TEST_USER_HEADER = {"x-user-id": "triggers_tests_user"}


def test_buy_trigger():
    """
    Tests setting up a buy trigger.
    Does not test the trigger being executed, as I am not sure how to do that.
    """


def test_sell_trigger():
    """
    Tests setting up a sell trigger.
    """
