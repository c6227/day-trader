from fastapi.testclient import TestClient

from ..main import app

TEST_USER_HEADER = {"x-user-id": "stocks_tests_user"}


def test_quote():
    """
    Tests quoting a stock
    """
    with TestClient(app) as client:
        response = client.get("/stocks/quote/ACA", headers=TEST_USER_HEADER)

        # the stock price is random so there's no way to test the values
        json = response.json()

        price = json["price"]
        assert type(price) == float or type(price) == int
        assert "crypto_key" in json


def test_quote_caching():
    """
    Tests quote caching
    """
    with TestClient(app) as client:
        response = client.get("/stocks/quote/ABC", headers=TEST_USER_HEADER)
        response2 = client.get("/stocks/quote/ABC", headers=TEST_USER_HEADER)

        # response2 should be from a cached price and have the same price and quote server crypto key
        assert response.json()["price"] == response2.json()["price"]
        assert response.json()["crypto_key"] == response2.json()["crypto_key"]


def test_buy_stock():
    """
    Test the process of staging and committing a buy, and cancellation.
    :return:
    """


def test_sell_stock():
    """
    Test the process of staging and committing a sell, and cancellation.
    :return:
    """
