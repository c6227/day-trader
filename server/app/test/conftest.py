"""
Pytest configuration module
"""
import asyncio
from os import environ

import motor.motor_asyncio


async def drop_existing_db():
    try:
        mongo_ip = environ["DT_MONGO_IP"]
    except KeyError:
        mongo_ip = "localhost"

    try:
        mongo_port = environ["DT_MONGO_PORT"]
    except KeyError:
        mongo_port = 28080

    mongodb_client = motor.motor_asyncio.AsyncIOMotorClient(mongo_ip, int(mongo_port))
    await mongodb_client.drop_database("daytrader")


# noinspection PyUnusedLocal
def pytest_configure(config):
    """
    Called before running tests
    """
    loop = asyncio.get_event_loop()
    loop.run_until_complete(drop_existing_db())
