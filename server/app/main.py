import os
from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse
from fastapi.openapi.utils import get_openapi
from starlette.responses import JSONResponse

from .databases import db_clients
from .log_queue import ErrorEventTypeNode, push_log_nodes, get_current_transaction_num

# import routers
from .routers import account
from .routers import logging
from .routers import stocks
from .routers import triggers

# maps endpoints to commands
# if the route STARTS WITH these strings assume the mapped command was used (for error logging)
# evaluated in order, the first match is taken
route_command_map = {
    "/account/add/": "ADD",
    "/account/summary": "DISPLAY_SUMMARY",
    "/logging/dumplog": "DUMPLOG",
    "/stocks/quote/": "QUOTE",
    "/stocks/buy/commit": "COMMIT_BUY",
    "/stocks/buy/cancel": "CANCEL_BUY",
    "/stocks/buy/": "BUY",
    "/stocks/sell/commit": "COMMIT_SELL",
    "/stocks/sell/cancel": "CANCEL_SELL",
    "/stocks/sell/": "SELL",
    "/triggers/buy/quantity/": "SET_BUY_AMOUNT",
    "/triggers/buy/target/": "SET_BUY_TRIGGER",
    "/triggers/buy/cancel": "CANCEL_SET_BUY",
    "/triggers/sell/quantity/": "SET_SELL_AMOUNT",
    "/triggers/sell/target/": "SET_SELL_TRIGGER",
    "/triggers/sell/cancel": "CANCEL_SET_SELL",

    # default
    "/": "UNKNOWN_CMD"
}

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    await db_clients.set_up()


@app.middleware("http")
async def require_userid(request: Request, call_next):
    # these paths do not require X-User-Id
    whitelist = [
        "/",
        "/logging/dumplog",
        "/docs"
    ]

    if request.url.path in whitelist:
        return await call_next(request)

    # this is still causing problems

    # if "x-user-id" not in request.headers:
    #    return JSONResponse({"detail": "x-user-id is required for this endpoint."}, status_code=400)

    # create the user if they don't exist
    db = db_clients.mongodb.daytrader
    user_id = request.headers.get("x-user-id")
    user = await db.users.find_one({"userID": user_id})
    if user is None:
        await db.users.insert_one({"userID": user_id, "funds": 0})

    return await call_next(request)


@app.middleware("http")
async def log_errors(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception as error:
        if "PYTEST_CURRENT_TEST" in os.environ:
            raise

        # get cmd from route
        last_command = None
        for key in route_command_map:
            if request.url.path.startswith(key):
                last_command = route_command_map[key]

        error_node = ErrorEventTypeNode(
            await get_current_transaction_num(),
            last_command,
            request.headers.get("X-User-Id", None),
            error_message=repr(error)
        )
        await push_log_nodes([error_node])

        # raise the exception as usual to cause a 500 error
        raise


@app.get("/")
async def index():
    return RedirectResponse("/docs")


# include imported routers
app.include_router(account.router, prefix="/account")
app.include_router(logging.router, prefix="/logging")
app.include_router(stocks.router, prefix="/stocks")
app.include_router(triggers.router, prefix="/triggers")


# openapi stuff
def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Day Trader Transaction API",
        version="0.1.0",
        description="",
        routes=app.routes,
    )

    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
