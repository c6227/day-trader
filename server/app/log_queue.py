"""
Every command will generate a LogTypeNode
Not all attributes in a LogTypeNode need to be filled out
example: an ADD command on has "user_command" and "account_transactions"
Non-filled attributes are given the None type
"""
from time import time
from os import environ
from xml.dom import minidom
from xml.etree.ElementTree import Element, tostring
from typing import Union, Optional
from .databases import db_clients


SERVER_NAME = environ.get("DT_SERVER_NAME", "unnamed")


class BaseLogNode:
    """
    Defines base methods for all log node types
    """
    def get_username(self):
        if hasattr(self, "_username"):
            if self._username is None:
                return None

            return self._username.text

        return None

    def get_element(self):
        raise NotImplementedError


# some attributes may be None if they are not needed
class UserCommandTypeNode(BaseLogNode):
    def __init__(
        self,
        transaction_num,
        command,
        username=None,
        stock_symbol=None,
        file_name=None,
        funds=None,
    ):
        # create Elements from each prop
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._command = _prop_to_element(command, "command")
        self._username = _prop_to_element(username, "username")
        self._stock_symbol = _prop_to_element(stock_symbol, "stockSymbol")
        self._file_name = _prop_to_element(file_name, "filename")
        self._funds = _prop_to_element(funds, "funds")

    def get_element(self):
        # get an element with children for this xml node
        root = Element("userCommand")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._command)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._stock_symbol)
        _append_if_not_none(root, self._file_name)
        _append_if_not_none(root, self._funds)
        return root


# some attributes may be None if they are not needed
class QuoteServerTypeNode(BaseLogNode):
    def __init__(
        self,
        transaction_num,
        price,
        stock_symbol,
        username,
        quote_server_time,
        crypto_key,
    ):
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._price = _prop_to_element(price, "price")
        self._stock_symbol = _prop_to_element(stock_symbol, "stockSymbol")
        self._username = _prop_to_element(username, "username")
        self._quote_server_time = _prop_to_element(quote_server_time, "quoteServerTime")
        self._crypto_key = _prop_to_element(crypto_key, "cryptokey")

    def get_element(self):
        root = Element("quoteServer")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._price)
        _append_if_not_none(root, self._stock_symbol)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._quote_server_time)
        _append_if_not_none(root, self._crypto_key)
        return root


# some attributes may be None if they are not needed
class AccountTransactionTypeNode(BaseLogNode):
    def __init__(self, transaction_num, action, username, funds):
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._action = _prop_to_element(action, "action")
        self._username = _prop_to_element(username, "username")
        self._funds = _prop_to_element(funds, "funds")

    def get_element(self):
        root = Element("accountTransaction")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._action)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._funds)
        return root


# some attributes may be None if they are not needed
class SystemEventTypeNode(BaseLogNode):
    def __init__(
        self,
        transaction_num,
        command,
        username=None,
        stock_symbol=None,
        file_name=None,
        funds=None,
    ):
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._command = _prop_to_element(command, "command")
        self._username = _prop_to_element(username, "username")
        self._stock_symbol = _prop_to_element(stock_symbol, "stockSymbol")
        self._file_name = _prop_to_element(file_name, "filename")
        self._funds = _prop_to_element(funds, "funds")

    def get_element(self):
        root = Element("systemEvent")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._command)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._stock_symbol)
        _append_if_not_none(root, self._file_name)
        _append_if_not_none(root, self._funds)
        return root


# some attributes may be None if they are not needed
# noinspection DuplicatedCode
class ErrorEventTypeNode(BaseLogNode):
    def __init__(
        self,
        transaction_num,
        command,
        username=None,
        stock_symbol=None,
        file_name=None,
        funds=None,
        error_message=None,
    ):
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._command = _prop_to_element(command, "command")
        self._username = _prop_to_element(username, "username")
        self._stock_symbol = _prop_to_element(stock_symbol, "stockSymbol")
        self._file_name = _prop_to_element(file_name, "filename")
        self._funds = _prop_to_element(funds, "funds")
        self._error_message = _prop_to_element(error_message, "errorMessage")

    def get_element(self):
        root = Element("errorEvent")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._command)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._stock_symbol)
        _append_if_not_none(root, self._file_name)
        _append_if_not_none(root, self._funds)
        _append_if_not_none(root, self._error_message)
        return root


# some attributes may be None if they are not needed
# noinspection DuplicatedCode
class DebugTypeNode(BaseLogNode):
    def __init__(
        self,
        transaction_num,
        command,
        username=None,
        stock_symbol=None,
        file_name=None,
        funds=None,
        debug_message=None,
    ):
        self._timestamp = _prop_to_element(_get_timestamp(), "timestamp")
        self._server = _prop_to_element(SERVER_NAME, "server")
        self._transaction_num = _prop_to_element(transaction_num, "transactionNum")
        self._command = _prop_to_element(command, "command")
        self._username = _prop_to_element(username, "username")
        self._stock_symbol = _prop_to_element(stock_symbol, "stockSymbol")
        self._file_name = _prop_to_element(file_name, "filename")
        self._funds = _prop_to_element(funds, "funds")
        self._debug_message = _prop_to_element(debug_message, "debugMessage")

    def get_element(self):
        root = Element("errorEvent")
        _append_if_not_none(root, self._timestamp)
        _append_if_not_none(root, self._server)
        _append_if_not_none(root, self._transaction_num)
        _append_if_not_none(root, self._command)
        _append_if_not_none(root, self._username)
        _append_if_not_none(root, self._stock_symbol)
        _append_if_not_none(root, self._file_name)
        _append_if_not_none(root, self._funds)
        _append_if_not_none(root, self._debug_message)
        return root


class _LogQueue:
    async def append(self, xml_str: str, user_id: Optional[str] = None):
        """
        Append to the distributed log buffer
        :param xml_str: the XML to append
        :param user_id: if specified, append to the log for this user id as well
        """
        redis = db_clients.redis

        if user_id is not None:
            await redis.rpush(f"xml_log_{user_id}", xml_str)

        await redis.rpush(f"xml_log", xml_str)

    async def dump(self, user_id: Optional[str] = None):
        """
        Get an entire log. This will NOT include the xml declaration or the root <log> element
        :param user_id: if specified, dump the log for this user_id
        :return: The xml string excluding declaration and root element. Will be None if there is no log data.
        """
        redis = db_clients.redis

        key = "xml_log"
        if user_id is not None:
            key = f"xml_log_{user_id}"

        # async with redis.pipeline(transaction=True) as pipe:
        #     get_res, del_res = await (pipe.lrange(key, 0, -1).delete(key).execute())

        log = await redis.lrange(key, 0, -1)
        await redis.delete(key)

        file = open("temp_log.xml", "w+")

        file.write("<?xml version=\"1.0\"?>\n<log>\n\n")

        for entry in log:
            file.write(entry + "\n")

        file.write("</log>")

        return file
        # return log


log_queue = _LogQueue()


_log_types = Union[
    UserCommandTypeNode,
    QuoteServerTypeNode,
    AccountTransactionTypeNode,
    SystemEventTypeNode,
    ErrorEventTypeNode,
    DebugTypeNode
]


async def push_log_nodes(nodes: list[_log_types]):
    """
    Push one or more log nodes to be added to the current log buffer.
    """
    for node in nodes:
        username = node.get_username()
        await log_queue.append(_pretty_xml(node.get_element()), username)


async def get_current_transaction_num():
    redis = db_clients.redis
    num = await redis.get("trans_num")
    if num is None:
        return 0

    return int(num)


async def increment_transaction_num():
    redis = db_clients.redis
    await redis.incr("trans_num", 1)  # "If no key exists, the value will be initialized as amount"


def _prop_to_element(prop: Union[any, None], tag_name: str):
    """
    Convert a class property to an ElementTree element. if 'prop' is none, returns None.
    prop will be cast to str
    tag_name is the xml tag name
    """
    if prop is None:
        return None

    el = Element(tag_name)
    el.text = str(prop)
    return el


def _append_if_not_none(parent: Element, child: Element):
    if child is not None:
        parent.append(child)


def _pretty_xml(element: Element):
    """
    Returns a pretty (indented and newlined) xml string from a ElementTree Element
    """
    rough_string = tostring(element, "utf-8")
    dom_parsed = minidom.parseString(rough_string)
    top_element = dom_parsed.childNodes[0]
    return top_element.toprettyxml(indent="  ")


def _get_timestamp():
    return int(time() * 1000)
