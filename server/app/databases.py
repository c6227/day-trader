import asyncio
from os import environ
import aioredis
import motor.motor_asyncio

try:
    REDIS_IP = environ["DT_REDIS_IP"]
except KeyError:
    REDIS_IP = "localhost"

try:
    REDIS_PORT = environ["DT_REDIS_PORT"]
except KeyError:
    REDIS_PORT = "6379"

try:
    MONGO_IP = environ["DT_MONGO_IP"]
except KeyError:
    MONGO_IP = "localhost"

try:
    MONGO_PORT = environ["DT_MONGO_PORT"]
except KeyError:
    MONGO_PORT = 28080


class _AsyncDatabaseClients:
    """
    Singleton that sets up and holds reference to async database interfaces. Provides an async context rather than
    instantiating in global scope.

    Ref: https://fastapi.tiangolo.com/advanced/async-tests/#other-asynchronous-function-calls
    """
    def __init__(self):
        self.redis = None
        self.mongodb = None

    async def set_up(self):
        self.redis = aioredis.from_url(
            f"redis://{REDIS_IP}:{REDIS_PORT}", encoding="utf-8", decode_responses=True
        )
        self.mongodb = motor.motor_asyncio.AsyncIOMotorClient(MONGO_IP, int(MONGO_PORT))


db_clients = _AsyncDatabaseClients()
