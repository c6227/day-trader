import os
import asyncio
from celery import Celery
from app.databases import db_clients
from app.quote_server import get_quote
from app.log_queue import (
    AccountTransactionTypeNode,
    SystemEventTypeNode,
    push_log_nodes,
    get_current_transaction_num,
    increment_transaction_num
)

loop = asyncio.get_event_loop()


celery = Celery('server')

redis_ip = os.environ["DT_REDIS_IP"]
redis_port = os.environ["DT_REDIS_PORT"]

celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", f"redis://{redis_ip}:{redis_port}")
celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", f"redis://{redis_ip}:{redis_port}")
#celery.conf.update(task_ack_late = True, worker_prefetch_multiplier = 1)


async def triggers():
    await db_clients.set_up()
    db = db_clients.mongodb.daytrader

    # Checks for any active triggers
    valid_buy_triggers = db.buyTriggers.find({"active": 1})
    for trigger in await valid_buy_triggers.to_list(length=None):
        symbol = trigger["symbol"]
        x_user_id = trigger["userID"]
        quoted = await get_quote(symbol, x_user_id)
        if trigger["trigger"] >= quoted.price:
            purchase_amount = trigger["amount"]
            trig_id = trigger["_id"]
            existing = await db.stocksOwned.find_one({"userID": x_user_id, "symbol": symbol})
            await increment_transaction_num()
            transaction_num = await get_current_transaction_num()
            # system_event = SystemEventTypeNode(transaction_num, "buy trigger activated", x_user_id, symbol, purchase_amount)
            # await push_log_nodes([system_event])
            if existing:
                await db.stocksOwned.update_one({"_id": trig_id}, {"$inc": {"amount": purchase_amount}})
            else:
                await db.stocksOwned.insert_one({"userID": x_user_id, "symbol": symbol, "amount": purchase_amount})
            await db.buyTriggers.delete_one({"_id": trig_id})

    valid_sell_triggers = db.sellTriggers.find({"active": 1})
    for trigger in await valid_sell_triggers.to_list(length=None):
        symbol = trigger["symbol"]
        x_user_id = trigger["userID"]
        quoted = await get_quote(symbol, x_user_id)
        if trigger["trigger"] <= quoted.price:
            sell_amount = trigger["amount"]
            await db.users.update_one({"userID": x_user_id}, {"$inc": {"funds": sell_amount}})
            await db.sellTriggers.delete_one({"_id": trigger["_id"]})
            await increment_transaction_num()
            transaction_num = await get_current_transaction_num()
            transaction = AccountTransactionTypeNode(transaction_num, "add", x_user_id, sell_amount)
            # system_event = SystemEventTypeNode(transaction_num, "sell trigger activated", x_user_id, symbol, sell_amount)
            # await push_log_nodes([transaction, system_event])

@celery.task
def check_triggers():
    loop.run_until_complete(triggers())

celery.conf.beat_schedule = {
    'check-triggers-every-30-seconds': {
        'task': 'tasks.check_triggers',
        'schedule': 30.0
    },
}
celery.conf.timezone = 'UTC'