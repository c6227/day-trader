# Transaction Server

This implementation of the transaction server uses FastAPI, the project was 
initialized using the instructions found 
[here](https://fastapi.tiangolo.com/deployment/docker/#build-a-docker-image-for-fastapi).

## Environment Variables

```shell
DT_QUOTESERV_IP # IP address of the legacy quote server
DT_QUOTESERV_PORT # Port of the legacy quote server
DT_MONGO_IP # IP of mongo
DT_MONGO_PORT # Port of mongo
DT_REDIS_IP # IP id of redis
DT_REDIS_PORT # port of redis
DT_SERVER_NAME # name of server for log files
```

## Commands

### Build and Start the Docker Image

From this directory:

```shell
docker build -t transactionserver .
docker run -d -p 8000:80 transactionserver
```

Should get hello world at http://localhost:8000/

### Run locally

This is faster and supports hot-reloading.

Setup with venv:

```shell
python -m venv ./venv

# mac/linux
./venv/bin/activate

# windows
.\venv\Scripts\activate

pip install -r requirements.txt
```

Run the app:

```shell
python -m uvicorn main:app --reload
```

Should get hello world at http://localhost:8000/
