# Locust
Load testing tool.

### Docker

When using `docker-compose.test.yml` a locust image is started and can be accessed
at http://localhost:8089 in a web browser.

### Standalone

If starting docker-compose.test.yml you need to stop the locust container first.


Running it this way seems to support more requests per second from the locust 
clients (more cpu allocation?).

```shell
# get the locust python package
pip3 install locust

# run the tests (assuming the backend server is running)
cd locust && locust -H http://localhost:8080
```

Then open http://localhost:8089 in a browser to access the locust interface.