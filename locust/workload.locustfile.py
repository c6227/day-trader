"""
This workload locustfile can run a json formatted workload called json_workload.json in the same directory.
"""

import json
import resource
from locust import events, HttpUser, task, between, constant

json_workload = {}

resource.setrlimit(resource.RLIMIT_NOFILE, (999999, 999999))


@events.init.add_listener
def on_locust_init(environment, **kwargs):
    # load the json workload into memory
    with open("json_workload.json", "r") as jsonfile:
        json_workload.update(json.load(jsonfile))
        print(f"{len(json_workload)} user workloads loaded. Use this number as the peak users!!")


class WorkloadUser(HttpUser):
    wait_time = between(0.1, 0.2)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # claim a user's workload
        self.user_id = None
        self.workload = None

        users = list(json_workload.keys())
        if len(users) < 1:
            print("A WorkloadUser was created but there are no more workloads from the file!")
            self.wait_time = constant(100000)
            return

        self.user_id = users[0]
        self.workload = json_workload[self.user_id]
        del json_workload[self.user_id]

        self.client.headers.update({"x-user-id": self.user_id})

        # if the user_id and workload are still "None" (json_workload was empty) this user won't have anything to do
        # when starting locust, the peak users should be set to how many unique users are specified in the workload

    @task(1)
    def work(self):
        if self.user_id is None:
            # nothing to do
            return

        if len(self.workload) < 1:
            # no more requests in queue, this user is done
            print("A WorkloadUser has finished their tasks.")
            self.wait_time = constant(100000)
            self.user_id = None
            return

        # do next request
        request = self.workload.pop(0)
        self.client.request(request["method"].upper(), request["url"])
