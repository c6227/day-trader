import re
import sys

username_regex = re.compile(r"\[\d+]\s+\w+,(\w+)")


def main():
    if len(sys.argv) < 2:
        print("Please specify the input logfile as an argument.")
        return -1

    filename = sys.argv[1]

    with open(filename) as file:
        for line in file:
            line = line.rstrip()

            try:
                username = username_regex.search(line)[1]
            except (TypeError, IndexError):
                username = "no_user_match"

            with open(f"split_{username}.txt", "a+") as outfile:
                outfile.write(line + "\n")


if __name__ == "__main__":
    main()
