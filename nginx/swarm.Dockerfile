FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.swarm.conf /etc/nginx/conf.d/default.conf