"""
Fake quote server.
"""

from time import sleep, time
from random import uniform
import asyncio


class EchoServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info("peername")
        print(f"Connection from {peername}")
        self.transport = transport

    def data_received(self, data):
        peername = self.transport.get_extra_info('peername')
        message = data.decode()
        split = message.split()
        if not len(split) == 2:
            print(f"Bad message from {peername}")
            return

        # sleep(uniform(0, 1))

        sym = split[0]

        self.transport.write(
            f"{uniform(20, 200)},{sym},NA,{int(time() * 1000)},CRYPTO_KEY_{int(uniform(1000,2000))}\n".encode()
        )

        print(f"Responded to f{peername}")

    def connection_lost(self, exc):
        peername = self.transport.get_extra_info("peername")
        print(f"{peername} closed connection.")
        self.transport.close()


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    server = await loop.create_server(
        lambda: EchoServerProtocol(),
        "0.0.0.0", 8888)

    async with server:
        await server.serve_forever()


asyncio.run(main())
