# Fake quote server

Simple fake quote server designed to sit in place of the legacy quote server so testing can be done
outside of uvic's server. It simply waits for a random amount of time up to 1 second before responding
with a random price between 20 and 200.