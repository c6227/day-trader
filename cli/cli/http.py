"""
Request session
"""
import json.decoder

import requests
from typer import echo
from .state_manager import state_manager


class _Session(requests.Session):
    def __init__(self, url_base=None):
        super(_Session, self).__init__()
        self.url_base: str = url_base

    def request(self, method, url, **kwargs):
        # update user id header before each request
        self.headers.update({"x-user-id": state_manager.config["user_id"]})
        self.headers.update({"accept": "application/json"})

        # remove trailing slashes to prevent 404
        while self.url_base.endswith("/"):
            self.url_base = self.url_base[:-1]

        modified_url = self.url_base + url
        resp = None

        # handle bad responses according to fastapi convention
        try:
            resp = super(_Session, self).request(method, modified_url, **kwargs)
            resp.raise_for_status()
            return resp
        except requests.HTTPError:
            if resp is not None:
                try:
                    data = resp.json()
                except json.decoder.JSONDecodeError:
                    data = {}

                if "detail" in data:
                    echo(f"The operation failed (status code {resp.status_code}): {data['detail']}")
                else:
                    echo(f"The operation failed (status code {resp.status_code})")

            return None

    def set_base_url(self, url):
        self.url_base = url


session = _Session(state_manager.config["server_url"])
