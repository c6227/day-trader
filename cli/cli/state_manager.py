"""
State manager
"""
import json
from pathlib import Path

import typer


class _StateManager:
    """
    Singleton state manager
    """

    def __init__(self):
        config_dir = Path(typer.get_app_dir("daytrade-cli"))
        config_dir.mkdir(exist_ok=True)

        self.config_path: Path = config_dir / "config.json"
        self.config = {
            "user_id": "testuser",
            "server_url": "http://localhost:8080"
        }

    def load_config(self):
        if not self.config_path.is_file():
            return

        with open(self.config_path, "r") as config_file:
            new_config = json.load(config_file)
            for key in new_config:
                if key not in self.config:
                    continue

                self.config[key] = new_config[key]

    def save_config(self):
        with open(self.config_path, "w+") as config_file:
            json.dump(self.config, config_file)


state_manager = _StateManager()
