"""
Stocks commands
"""
from typer import echo
from ..http import session
from ..util import format_money


def quote(symbol: str):
    """
    Get a quote for the current price of 'symbol'
    """
    resp = session.get(f"/stocks/quote/{symbol}")
    if resp:
        resp = resp.json()
        echo(f"Price of {symbol} is ${format_money(resp['price'])}.")


def stage_buy(symbol: str, amount: float):
    """
    Stage a buy for 'amount' of 'symbol'
    """
    resp = session.post(f"/stocks/buy/{symbol}/{amount}")
    if resp:
        resp = resp.json()
        echo(
            f"Staged a buy for ${format_money(resp['amount'])} of {resp['symbol']}"
        )


def commit_buy():
    """
    Commit a staged buy
    """
    resp = session.post(f"/stocks/buy/commit")
    if resp:
        resp = resp.json()
        echo(
            f"Purchased ${format_money(resp['amount_purchased'])} of {resp['symbol']}. " +
            f"Account balance is now ${format_money(resp['new_account_balance'])}"
        )


def cancel_buy():
    """
    Cancel a staged buy
    """
    resp = session.post(f"/stocks/buy/cancel")
    if resp:
        resp = resp.json()
        echo(f"Cancelled a buy for ${format_money(resp['cancelled_amount'])} of {resp['cancelled_symbol']}")


def stage_sell(symbol: str, amount: float):
    """
    Stage a sell for 'amount' shares of 'symbol'
    """
    resp = session.post(f"/stocks/sell/{symbol}/{amount}")
    if resp:
        resp = resp.json()
        echo(
            f"Staged a sale of ${format_money(resp['amount'])} of {resp['symbol']}. "
        )


def commit_sell():
    """
    Commit a staged sell
    """
    resp = session.post(f"/stocks/sell/commit")
    if resp:
        resp = resp.json()
        echo(
            f"Sold ${format(resp['amount_sold'])} of {resp['symbol']}. " +
            f"Account balance is now ${format_money(resp['new_account_balance'])}"
        )


def cancel_sell():
    """
    Cancel a staged sell
    """
    resp = session.post(f"/stocks/sell/cancel")
    if resp:
        resp = resp.json()
        echo(f"Cancelled a sell for ${format_money(resp['cancelled_amount'])} of {resp['cancelled_symbol']}")
