"""
Triggers commands
"""
from typer import echo
from ..http import session
from ..util import format_money


def set_buy_trigger(symbol: str, price: float):
    """
    Set trigger price for an autobuy
    The server will return an error if set-buy-amount was not used yet
    """
    resp = session.post(f"/triggers/buy/target/{symbol}/{price}")
    if resp:
        resp = resp.json()
        echo(
            f"Auto-buy for {resp['symbol']} confirmed for when the price is less than or "
            f"equal to ${format_money(resp['price'])}"
        )


def set_buy_amount(symbol: str, amount: float):
    """
    Set amount (quantity) for an auto-buy.
    """
    resp = session.post(f"/triggers/buy/quantity/{symbol}/{amount}")
    if resp:
        resp = resp.json()
        echo(
            f"Setting an auto-buy for ${format_money(resp['amount'])} of {resp['symbol']}.\n"
            + f"Your account balance is now ${format_money(resp['new_account_balance'])}.\n"
            f"Use the set-buy-trigger command to set the trigger price."
        )


def set_buy_cancel(symbol: str):
    """
    Cancels any pending auto-buy commands.
    """
    resp = session.post(f"/triggers/buy/cancel/{symbol}")
    if resp:
        resp = resp.json()
        echo(
            f"Cancelled the pending auto-buy for {resp['symbol']}. ${format_money(resp['amount_returned'])} " +
            f"has been returned to your account."
        )


def set_sell_trigger(symbol: str, price: float):
    """
    Set trigger price for an auto-sell
    The server will return an error if set-sell-amount was not used yet
    """
    resp = session.post(f"/triggers/sell/target/{symbol}/{price}")
    if resp:
        resp = resp.json()
        echo(
            f"Auto-sell for {resp['symbol']} confirmed for when the price is greater than or "
            f"equal to ${format_money(resp['minimum_stock_sell_price'])}"
        )


def set_sell_amount(symbol: str, amount: float):
    """
    Set amount (quantity) for an auto-sell.
    """
    resp = session.post(f"/triggers/sell/quantity/{symbol}/{amount}")
    if resp:
        resp = resp.json()
        echo(
            f"Setting an auto-sell for ${format_money(resp['stocks_to_sell'])} of {resp['symbol']}.\n"
            + f"You now have ${format_money(resp['total_stocks'])} of {resp['symbol']}.\n"
              f"Use the set-sell-trigger command to set the trigger price."
        )


def set_sell_cancel(symbol: str):
    """
    Cancels any pending auto-sell commands.
    """
    resp = session.post(f"/triggers/sell/cancel/{symbol}")
    if resp:
        resp = resp.json()
        echo(
            f"Cancelled the pending auto-sell for {resp['symbol']}. ${format_money(resp['stocks_returned'])} " +
            f"of {resp['symbol']} has been returned to your account."
        )
