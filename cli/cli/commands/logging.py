"""
Logging commands
"""
from typer import Option
from typer.models import OptionInfo
from ..http import session


def dump_log(file: str, user: bool = Option(False, help="Show only logged actions from your userid.")):
    """
    Dump log events to stdout.
    """
    output = ""

    if not user or isinstance(user, OptionInfo):
        resp = session.get("/logging/dumplog", headers={'accept': 'application/xml'})
        if resp:
            output += resp.text
    else:
        resp = session.get("/logging/dumplog/user", headers={'accept': 'application/xml'})
        if resp:
            output += resp.text

    if len(output) > 0:
        with open(file+".xml", "w+") as out_file:
            out_file.write(output)
