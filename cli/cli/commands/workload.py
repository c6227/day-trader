"""
Workload file commands
"""
import json
import re
from typer import echo


_format_line = lambda x: re.sub(r"\[\d+]\s", "", x).rstrip()
_username_regex = re.compile(r"\[\d+]\s+\w+,(\w+)")

# map commands to http methods and unformatted urls
_command_route_map = {
    "ADD": ("post", "/account/add/%s"),
    "QUOTE": ("get", "/stocks/quote/%s"),
    "BUY": ("post", "/stocks/buy/%s/%s"),
    "COMMIT_BUY": ("post", "/stocks/buy/commit"),
    "CANCEL_BUY": ("post", "/stocks/buy/cancel"),
    "SELL": ("post", "/stocks/sell/%s/%s"),
    "COMMIT_SELL": ("post", "/stocks/sell/commit"),
    "CANCEL_SELL": ("post", "/stocks/sell/cancel"),
    "SET_BUY_AMOUNT": ("post", "/triggers/buy/quantity/%s/%s"),
    "SET_BUY_TRIGGER": ("post", "/triggers/buy/target/%s/%s"),
    "CANCEL_SET_BUY": ("post", "/triggers/buy/cancel/%s"),
    "SET_SELL_AMOUNT": ("post", "/triggers/sell/quantity/%s/%s"),
    "SET_SELL_TRIGGER": ("post", "/triggers/sell/target/%s/%s"),
    "CANCEL_SET_SELL": ("post", "/triggers/sell/cancel/%s"),
    "DISPLAY_SUMMARY": ("get", "/account/summary")
}


def _process_command(cmd: str):
    """
    :param cmd: the command string
    :return: ("get" | "post", formatted_url), None if no request is needed
    """
    cmdv = cmd.split(",")

    # command name
    command = cmdv[0]

    # args
    cmd_args = cmdv[2:]

    if command == "DUMPLOG":
        # don't do anything with dumplog, dump it manually at the end
        return None

    method, url = _command_route_map[command]
    return method, url % tuple(cmd_args)


def workload(file: str):
    """
    Convert a workload file to a json file our locustfile can run
    """
    with open(file, "r") as workload_file:
        # split into a command list for each unique user
        command_lists = {}

        for line in workload_file:
            line = line.rstrip()

            try:
                username = _username_regex.search(line)[1]
            except (TypeError, IndexError):
                username = "no_user_match"

            method_and_url = _process_command(_format_line(line))
            if method_and_url is None:
                continue

            to_append = {
                "method": method_and_url[0],
                "url": method_and_url[1]
            }

            if username in command_lists:
                command_lists[username].append(to_append)
            else:
                command_lists[username] = [to_append]

        echo(f"Writing {len(command_lists)} unique user workload file json fomatted to json_workload.json")

        with open("json_workload.json", "w+") as jsonfile:
            json.dump(command_lists, jsonfile)
