"""
Account commands
"""
from typer import echo
from ..http import session
from ..util import format_money


def add_funds(amount: float):
    """
    Add an amount of funds to your account
    """
    resp = session.post(f"/account/add/{amount}")
    if resp:
        resp = resp.json()
        echo(
            f"Successfully added ${format_money(resp['funds_added'])} to your account."
        )


def summary():
    """
    View account summary
    """
    resp = session.get(f"/account/summary")
    if resp:
        resp = resp.json()
        echo(f"\nAccount summary:\n")
        echo(f"Balance: {resp['current_balance']}\n")
        # echo(f"Transaction History: {resp['transaction_history']}\n")

        # buy triggers
        echo("Buy triggers: ")
        for btrigger in resp["buy_triggers"]:
            if btrigger["active"] != 1:
                continue

            echo(
                f"Buy ${format_money(btrigger['amount'])} of {btrigger['symbol']} "
                + f"when its price drops below ${format_money(btrigger['trigger'])}"
            )

        echo("--\n")

        # sell triggers
        echo("Sell triggers: ")
        for strigger in resp["sell_triggers"]:
            if strigger["active"] != 1:
                continue

            echo(
                f"Sell ${format_money(strigger['amount'])} of {strigger['symbol']} "
                + f"when its price goes above ${format_money(strigger['trigger'])}"
            )

        echo("--\n")

        # owned stocks
        echo(f"Stocks Owned: ")
        for owned in resp["stocks_owned"]:
            echo(f"{owned['symbol']} (${format_money(owned['amount'])})")

        echo("--\n")
