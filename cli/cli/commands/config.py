"""
Config commands
"""
import typer
from ..state_manager import state_manager


def config(key: str, value: str):
    """
    Set a config key and value
    """
    if key not in state_manager.config:
        typer.echo("That is not a valid configuration key!")
        return -1

    state_manager.config[key] = value
    state_manager.save_config()
