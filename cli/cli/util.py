"""
Utility functions
"""


def format_money(money: float):
    return "{:,.2f}".format(money)
