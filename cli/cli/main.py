import typer
from .commands import account, config, logging, stocks, triggers, workload
from .state_manager import state_manager
from .http import session

app = typer.Typer()

# register commands
app.command(name="add")(account.add_funds)
app.command(name="summary")(account.summary)
app.command(name="config")(config.config)
app.command(name="log")(logging.dump_log)

app.command(name="quote")(stocks.quote)
app.command(name="buy")(stocks.stage_buy)
app.command(name="buy-commit")(stocks.commit_buy)
app.command(name="buy-cancel")(stocks.cancel_buy)
app.command(name="sell")(stocks.stage_sell)
app.command(name="sell-commit")(stocks.commit_sell)
app.command(name="sell-cancel")(stocks.cancel_sell)

app.command(name="set-buy-amount")(triggers.set_buy_amount)
app.command(name="set-buy-trigger")(triggers.set_buy_trigger)
app.command(name="set-buy-cancel")(triggers.set_buy_cancel)
app.command(name="set-sell-amount")(triggers.set_sell_amount)
app.command(name="set-sell-trigger")(triggers.set_sell_trigger)
app.command(name="set-sell-cancel")(triggers.set_sell_cancel)

app.command(name="workload")(workload.workload)


@app.callback()
def main():
    """
    Day-trader CLI application
    """
    # initialize the state manager with deserialized configuration
    state_manager.load_config()

    # initialize the http session
    session.set_base_url(state_manager.config["server_url"])
