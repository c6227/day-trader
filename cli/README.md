# CLI Tool

(optional) set the config values:
```shell
python daytrade-cli.py config user_id testuser
python daytrade-cli.py config server_url http://localhost:8080
```

They both default to the values shown above.

## Run a workload

```shell
python daytrade-cli.py workload ./workloads/1user.txt
```

See other commands:

```shell
python daytrade-cli.py --help
```
